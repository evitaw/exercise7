<div class="form-group">
        <label for="{{ $name }}">{{ $name }}</label>
        <input type="{{ $type }}" 
               name="{{ $name }}" 
               value="{{ old($name, $value) }}"
               placeholder="{{ $name }}"
        />
        @if($errors->has('$name'))
            <div>{{ $errors->first($name) }}</div>
        @endif
    </div>
    