<form method="post" action="/login">
    @csrf
  
    <input type="email" name="email" placeholder="Email" />
    <input type="password" name="password" placeholder="Password"/>
    <label>
      <input type="checkbox" name="remember" /> Remember me
    </label>
  
    <button>Login</button>
  </div>
  
  @can('c-level-only')
  Message ini bisa dilihat oleh C-level officers
@endcan

@cannot('c-level-only')
  Message ini tidak bisa dilihat oleh C-level officers
@endcannot

{{-- <x-alert type="warning">
  perhatian
</x-alert> --}}

<x-text-input type="email" 
              name="email" 
              value="something@email.com"
/>


