{{ session('status') }}

<form method="post" action="/forgot-password">
    @csrf

    <input type="email" name="email" placeholder="Email"/>
    @error('email')
        {{ $message }}
    @enderror

    <button>Send password reset link</button>
</form>
