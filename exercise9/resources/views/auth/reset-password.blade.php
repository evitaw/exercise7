{{ session('status') }}

<form method="post" action="/reset-password">
  @csrf

  <input type="hidden" name="token" value="{{ request()->token }}"/>
  <input type="email" name="email" value="{{ request()->email }}" placeholder="Email"/>
  <input type="password" name="password" placeholder="Password"/>
  <input type="password" name="password_confirmation" placeholder="Password Confirmation"/>

    <button>Reset password</button>
</form>
