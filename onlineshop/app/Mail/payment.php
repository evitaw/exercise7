<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class payment extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Payment Ini Olshop';
    //public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($total)
    {
        $this->total=$total;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email-payment', ['total' => $this->total]);
    }
}
