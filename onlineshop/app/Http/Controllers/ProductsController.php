<?php

namespace App\Http\Controllers;
use App\Models\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function index()
    {
        // return view('products');
        $products = DB::table('products')
        ->where('status', '1')
        ->orderBy('name')
        ->paginate(100);
 
    		// mengirim data pegawai ke view index
		return view('products',['products' => $products]);
    }
    public function cart()
    {
        return view('cart');
    }
    public function addToCart($id)
    {
        $products = Product::find($id);
        if(!$products) {
            abort(404);
        }
        $cart = session()->get('cart');
        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                    $id => [
                        "name" => $products->name,
                        "quantity" => 1,
                        "price" => $products->price,
                        "photo" => $products->photo
                    ]
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $products->name,
            "quantity" => 1,
            "price" => $products->price,
            "photo" => $products->photo
        ];
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }
    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }

    public function cari(Request $request)
    {
        $products = Product::where([
            ['name', '!=', NULL],
            [function ($query) use ($request) {
                if (($term = $request->term)) {
                    $query->orWhere('name', 'LIKE', '%' . $term . '%')->get();

                }
            }]
        ])
        ->orderBy("name")
        ->paginate(100);

        return view('crud-product', compact('products'))
        ->with('i', (request()->input('page',1)-1)*5);
    }

    public function tambah()
	{
 
		// memanggil view tambah
		return view('tambah-produk');
 
	}
 
	
	public function store(Request $request)
	{
        $destinationPath = 'uploads';
        $file = $request->file('photo')->store($destinationPath);
		
		DB::table('products')->insert([
			'name' => $request->name,
			'description' => $request->description,
			'photo' => $file,
            'price' => $request->price,
            'status' => $request->status
		]);
    
        //$file->move($destinationPath,$file->getClientOriginalName());
		return redirect('crud-product');
 
    }
    
    public function edit($id)
	{
		// mengambil data pegawai berdasarkan id yang dipilih
		$products = DB::table('products')->where('id',$id)->get();
		// passing data pegawai yang didapat ke view edit.blade.php
		return view('edit-produk',['products' => $products]);
 
    }
    
    public function updateProd(Product $products, Request $request)
	{
		// update data pegawai
		DB::table('products')->where('id',$request->id)->update([
			'name' => $request->name,
			'description' => $request->description,
			'photo' => $request->photo,
            'price' => $request->price,
            'status' => $request->status
		]);
		// alihkan halaman ke halaman pegawai
		return redirect('crud-product');
    }

    public function hapus($id)
	{
		// menghapus data pegawai berdasarkan id yang dipilih
		DB::table('products')->where('id',$id)->delete();
		
		// alihkan halaman ke halaman pegawai
		return redirect('crud-product');
    }
    
    public function show($id)
	{
		// menghapus data pegawai berdasarkan id yang dipilih
		$products = DB::table('products')->where('id',$id)->get();
		
		// alihkan halaman ke halaman pegawai
		return view('detail-produk',['products' => $products]);
	}
}
