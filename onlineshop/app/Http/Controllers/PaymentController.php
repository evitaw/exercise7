<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Mail\payment;

class PaymentController extends Controller
{
    public function show()
    {
        $total=0;
        $this->total=$total;
        dump(session('cart'));
        foreach(session('cart') as $id => $details)
        {
            $total += $details['price'] * $details['quantity']; 
        }
        
        return $this->view('invoice', ['total' => $this->total]);
    }

    public function store(Request $request)
	{
		
		DB::table('payment')->insert([
            'name' => $request->name,
            'street' => $request->street,
            'city' => $request->city,
            'poscode' => $request->poscode,
            'phone' => $request->phone,
            'bukti' => $request->bukti
        ]);

        $file = $request->file('bukti');
        $destinationPath = 'uploads';
        $file->move($destinationPath,$file->getClientOriginalName());
        
        $total = $request->name;
        
        $users = User::where('role',1)->get();
        foreach($users as $user) {
            Mail::to($user->email)->send(new payment($total));
        }
        
		return redirect('email-payment');
 
    }
 
}
