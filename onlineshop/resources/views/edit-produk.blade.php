<link rel="stylesheet" type="text/css" href="/css/app.css">

@extends('layout')

<h1 class="text-center">Ubah Produk</h1>

@section('title', 'Produk')

@section('content')
        <a class="btn btn-success" href="{{ url('crud-product') }}">
        Kembali
        </a>
   
        <div class="row justify-content-center align-items-center">
            <div class="card" style="width: 24rem;">
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @foreach($products as $p)
	
    <form action="{{ url('update-produk',$p->id) }}" method="post" id="myForm">
            <input type="hidden" name="id" value="{{ $p->id }}">
            @csrf
            @method('PUT')
                <div class="form-group">
                    <label for="name">Nama</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" value="{{ $p->name }}">                
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>                    
                    <input type="text" name="description" class="form-control" id="description" aria-describedby="description" value="{{ $p->description }}">   
                            </div>
                <div class="form-group">
                    <label for="photo">Foto</label>                    
                    <input type="text" name="photo" class="form-control" id="photo" aria-describedby="photo" value="{{ $p->photo }}">                
                </div>
                <div class="form-group">
                        <label for="price">Harga</label>                    
                        <input type="number" name="price" class="form-control" id="price" aria-describedby="price" value="{{ $p->price }}">                
                    </div>
                     
                    @can('isAdmin')
                    <div class="form-group">
                            <label for="status"><strong>Status</strong></label>
                            <select name="status" id="status" class="form-control" value="" aria-describedby="status">
                                    <option value="1" {{ $p->status == 1?"selected":"" }}>Aktif</option>    
                                <option value="2" {{ $p->status == 2?"selected":"" }}>Non Aktif</option>
                                    
                                  </select>
                        </div>
                @endcan
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
            @endforeach
                </div>
            </div>
        </div>
@endsection