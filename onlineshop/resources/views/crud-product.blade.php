<link rel="stylesheet" type="text/css" href="/css/app.css">

@extends('layout')

<h1 class="text-center">Kelola Produk</h1>

@section('title', 'Products')

@section('content')

   <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                    <a class="btn btn-success" href="{{ url('tambah-produk') }}">Tambah Produk</a>
                    <a class="btn btn-success" href="{{ url('impor-produk') }}">Impor File</a>
            </div>
            <div class="float-right my-2">
                <form action="crud-product" method="GET" class="form-inline">
                        <input class="form-control" type="text" name="term" placeholder="Cari Produk" value="{{ old('cari') }}" id="term">
                        <input class="btn btn-primary ml-3" type="submit" value="Cari">
                    </form>
            </div>
        </div>
    </div>
   
    {{-- @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif --}}
   
    <table class="table table-bordered">
            <tr align="center">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Foto</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                </tr>
        @foreach($products as $p)
            <tr>
                    <td align="center">{{ ++$i }}</td>
                <td>{{ $p->name }}</td>
                <td>{{ $p->description }}</td>
            <td><a href="{{ asset("storage/$p->photo") }}" target="_blank"><img src="{{ asset("storage/$p->photo") }}" alt="" height="100" width="100"></a></td>
                <td>{{ $p->price }}</td>
                        <td>
                                <a class="btn btn-warning btn-sm" href="edit-produk/{{ $p->id }}">Edit</a>
                                <a class="btn btn-danger btn-sm" href="hapus-produk/{{ $p->id }}">Hapus</a>
                            </td>
        </tr>
        @endforeach
    </table>
@endsection