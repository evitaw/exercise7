<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payment</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="col-md-4 offset-md-4 mt-5">
            <div class="card">
                <div class="card-header">
                    Halo, {{Auth::user()->name}}!
<p>
<p>

Total belanja yang harus Anda bayarkan sudah kami kirim melalui email. Pembayaran harap ditransfer ke rekening Ini Olshop.
Bukti transfer sekaligus alamat pengiriman dapat diunggah melalui di bawah ini.
<p><p>

Terima kasih!
                </div>
                {{ session("status") }}
                <form method="post" action="payment" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Nama Penerima">
                        <input type="text" name="street" class="form-control" placeholder="Alamat">
                        <input type="text" name="city" class="form-control" placeholder="Kota">
                        <input type="text" name="poscode" class="form-control" placeholder="Kode Pos">
                        <input type="text" name="phone" class="form-control" placeholder="Telepon">
                        <input type="file" name="bukti" class="form-control" placeholder="Bukti Bayar"> 
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-block">Kirim Bukti Bayar</button></div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>



