<link rel="stylesheet" type="text/css" href="/css/app.css">

@extends('layout')

<h1 class="text-center">Tambah Produk</h1>

@section('title', 'Produk')

@section('content')
        <a class="btn btn-success" href="{{ url('crud-product') }}">
        Kembali
        </a>
   
        <div class="row justify-content-center align-items-center">
            <div class="card" style="width: 24rem;">
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{ url('tambah-produk') }}" id="myForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="status" value="1">
                    <div class="form-group">
                        <label for="name">Nama</label>                    
                        <input type="text" name="name" class="form-control" id="name" aria-describedby="name" >                
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label>                    
                        <textarea class="form-control" name="description"></textarea>   
                                </div>
                    <div class="form-group">
                        <label for="photo">Foto</label>                    
                        <input type="file" name="photo">                
                    </div>
                    <div class="form-group">
                            <label for="price">Harga</label>                    
                            <input type="number" name="price" class="form-control" id="price" aria-describedby="price" >                
                        </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
                </div>
            </div>
        </div>
@endsection