<link rel="stylesheet" type="text/css" href="/css/app.css">

@extends('layout')

<h1 class="text-center">Detail Produk</h1>

@section('title', 'Produk')

@section('content')
        <a class="btn btn-success" href="{{ url('/') }}">
        Kembali
        </a>
   
        <div class="row justify-content-center align-items-center">
            <div class="card" style="width: 24rem;">
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @foreach($products as $p)
	
    
                <div class="form-group">
                    <label for="name">Nama</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" value="{{ $p->name }}">                
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>                    
                    <input type="text" name="description" class="form-control" id="description" aria-describedby="description" value="{{ $p->description }}">   
                            </div>
                <div class="form-group">
                    <label for="photo">Foto</label>                    
                    <input type="text" name="photo" class="form-control" id="photo" aria-describedby="photo" value="{{ $p->photo }}">                
                </div>
                <div class="form-group">
                        <label for="price">Harga</label>                    
                        <input type="number" name="price" class="form-control" id="price" aria-describedby="price" value="{{ $p->price }}">                
                    </div>
                     
            @endforeach
                </div>
            </div>
        </div>
@endsection