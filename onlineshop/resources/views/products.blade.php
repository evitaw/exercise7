<link rel="stylesheet" type="text/css" href="/css/app.css">

@extends('layout')

<h1 class="text-center">Ini Olshop</h1>

@section('title', 'Products')

@section('content')
<div class="container products">
@foreach($products->chunk(3) as $row)
<div class="row row-cols-3 row-cols-md-3">
        @foreach($row as $product)
        <div class="col-4 mb-4">
        <div class="card h-100">
          <img src="{{ $product->photo }}" class="card-img-top" alt="...">
          <div class="card-body">
            <a href="detail-produk/{{ $product->id }}"><h5 class="card-title">{{ $product->name }}</h5></a>
            <p class="card-text">{{ $product->description }}</p>
          </div>
          <div class="card-footer">
                <p class="card-text"><strong>Harga: </strong> Rp{{ $product->price }}</p>
          </div>
          @if(Auth::user()) 
                                    <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Beli</a> </p>
                                @endif
        </div>
    </div>
        @endforeach
</div>
      @endforeach
   </div>

@endsection