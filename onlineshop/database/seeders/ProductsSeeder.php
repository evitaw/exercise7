<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Jacquelle Sleeping Mask',
            'description' => 'Works like magic to nourish and hydrate, building a shield on the skin overnight for healthier, brighter skin in the morning.',
            'photo' => 'https://studio.femaledaily.com/7671-large_default/jacquelle-disney-princess-edition-facial-sleeping-mask-bundle.jpg',
            'price' => 154.08
         ]);
 
         DB::table('products')->insert([
             'name' => 'Azarine Sunscreen Gel',
             'description' => 'Formulated with natural ingredients Propolis, aloe Vera, green tea and pomegranate to protect the skin from UV A & UV B rays and nourish the skin.',
             'photo' => 'https://studio.femaledaily.com/7175-large_default/hydrasoothe-sunscreen-gel-spf-45.jpg',
             'price' => 65.00
         ]);
 
         DB::table('products')->insert([
             'name' => 'Buma Face Pallette',
             'description' => '3-shade palette with Bronzer / Contour , Blush, and Highlighter.',
             'photo' => 'https://studio.femaledaily.com/7067-large_default/asmara-face-palette.jpg',
             'price' => 188.00
         ]);
 
         DB::table('products')->insert([
             'name' => 'Sugarpot Body Powder',
             'description' => 'Its light, soft and silky texture is formulated to soften the skin and leave a refreshing scent all day long.',
             'photo' => 'https://studio.femaledaily.com/7038-large_default/powdra-body-power.jpg',
             'price' => 45.99
         ]);
 
         DB::table('products')->insert([
             'name' => 'Phyto Power Essence',
             'description' => 'Helpful for moisturizing the face, brightening, and giving fresh energy to the skin.',
             'photo' => 'https://studio.femaledaily.com/7031-large_default/phyto-power-essence-pre-serum.jpg',
             'price' => 200.00
         ]);
    }
}
