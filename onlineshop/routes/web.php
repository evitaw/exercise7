<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use GuzzleHttp\Psr7\Message;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PaymentController;
use App\Mail\invoice;
use App\Models\Product;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Common\Creator\WriterFactory;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function() {
//     return view('products', ['products' => \App\Models\Product::all()]);
// });

Route::get('/', [ProductsController::class, 'index']);

Route::patch('update-cart', [ProductsController::class, 'update']);

Route::delete('remove-from-cart', [ProductsController::class, 'remove']);

Route::get('cart', [ProductsController::class, 'cart']);

Route::get('add-to-cart/{id}', [ProductsController::class, 'addToCart']);

Route::get('login', function () {
    return view('login');
})->name('login');

Route::get('logout', function() {
    auth()->logout();
    session()->flush();
    return redirect('/');
});

Route::post('login', function () {
  
    $creds = request()->only('email', 'password');
    $remember = request()->remember;
  
    if (auth()->attempt($creds, $remember)) {
      return redirect('/');
    }
    
    return back()->with("status","Username/password salah");
  });
  
Route::get('forgot-password', function () {
    return view('auth.forgot-password');
})->middleware('guest')->name('password.request');


Route::get('reset-password/{token}', function ($token) {
    return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');

Route::post('forgot-password', function (Request $request) {
    $request->validate(['email' => 'required|email']);

    $status = Password::sendResetLink(
        $request->only('email')
    );

    return $status === Password::RESET_LINK_SENT
                ? back()->with(['status' => __($status)])
                : back()->withErrors(['email' => __($status)]);
})->middleware('guest')->name('password.email');

Route::post('reset-password', function (Request $request) {
    $request->validate([
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed',
    ]);

    $status = Password::reset(
        $request->only('email', 'password', 'password_confirmation', 'token'),
        function ($user, $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));

            $user->save();

            event(new PasswordReset($user));
        }
    );

    return $status === Password::PASSWORD_RESET
                ? redirect()->route('login')->with('status', __($status))
                : back()->withErrors(['email' => [__($status)]]);
})->middleware('guest')->name('password.update');

Route::get('registrasi', [AuthController::class, 'showFormRegistrasi'])->name('registrasi');

Route::post('registrasi', [AuthController::class, 'registrasi']);

Route::get('crud-product', [ProductsController::class, 'cari']);

Route::get('tambah-produk', [ProductsController::class, 'tambah']);

Route::post('tambah-produk', [ProductsController::class, 'store']);

Route::get('edit-produk/{id}',[ProductsController::class, 'edit']);

Route::put('update-produk/{id}', [ProductsController::class, 'updateProd']);

Route::get('hapus-produk/{id}',[ProductsController::class, 'hapus']);

Route::get('detail-produk/{id}',[ProductsController::class, 'show']);

Route::get('bayar', function () {
    $user = auth()->user();
    $total=0;
    foreach(session('cart') as $id => $details)
    {
                $total += $details['price'] * $details['quantity']; }

    Mail::to($user->email)->send(new invoice($total));
    session()->forget('cart');
    return redirect('payment');
});

Route::get('payment', function () {
    return view('payment');
});

Route::post('payment', [PaymentController::class, 'store']);

Route::get('email-payment', function () {
    return redirect('/');
});

Route::get('/impor-produk', function() {
    $file=base_path('produk.csv');
    $reader = ReaderEntityFactory::createReaderFromFile($file);
    $reader->open($file);
    foreach($reader->getSheetIterator() as $sheet) {
        foreach($sheet->getRowIterator() as $rowIndex => $row) {
            if($rowIndex==1) continue;
            foreach($row->getCells() as $cellIndex=>$cell) {
                if ($cellIndex == 1) $data[$rowIndex]['name'] = $cell->getValue();
                if ($cellIndex == 2) $data[$rowIndex]['description'] = $cell->getValue();
                if ($cellIndex == 3) $data[$rowIndex]['price'] = $cell->getValue();
                if ($cellIndex == 4) $data[$rowIndex]['status'] = $cell->getValue();
            }
        }
    }
    DB::table('products')->insert($data);
    $reader->close();
    return redirect('crud-product');
});

Route::get('/search', function () 
{
    $search = request();

    $result = Product::search($search)->get();

    return redirect('search');
});
