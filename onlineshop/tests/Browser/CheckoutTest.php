<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CheckoutTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/cart')
            ->assertPathIs('/cart')
            ->assertSeeLink('Bayar')
            ->clickLink('Bayar');
            // ->assertPathIs('/payment');
                // ->assertInputPresent('name');
                // ->assertSeeIn('button', 'Kirim Bukti Bayar');
            //     ->type('email', 'nbergnaum@example.net')
            //     ->type('password', '123')
            //     ->press('Login');
        });
    }
}
