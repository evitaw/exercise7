<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class EditProductTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/crud-product')
            ->assertPathIs('/crud-product')
            ->assertSeeLink('Edit')
            ->clickLink('Edit')
            ->assertPathIs('/edit-produk/2')
                ->assertInputPresent('name')
                ->assertInputPresent('description')
                ->assertInputPresent('photo')
                ->assertInputPresent('price')
                ->assertSeeIn('button', 'Simpan')
                ->type('name', 'produk')
                ->type('description', 'deskripsi')
                ->type('photo', 'https://studio.femaledaily.com/7175-large_default/hydrasoothe-sunscreen-gel-spf-45.jpg')
                ->type('price', '123')
                ->press('Simpan');
        });
    }
}
