<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AddProductTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/crud-product')
            ->assertPathIs('/crud-product')
            ->assertSeeLink('Tambah Produk')
            ->clickLink('Tambah Produk')
            ->assertPathIs('/tambah-produk')
                ->assertInputPresent('name')
                ->assertInputPresent('description')
                ->assertInputPresent('photo')
                ->assertInputPresent('price')
                ->assertSeeIn('button', 'Tambah')
                ->type('name', 'produk')
                ->type('description', 'deskripsi')
                ->attach('photo', public_path('/uploads/test.png'))
                ->type('price', '123')
                ->press('Tambah');
        });
    }
}
