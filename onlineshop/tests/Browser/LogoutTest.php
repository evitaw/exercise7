<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LogoutTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
            ->assertPathIs('/')
            ->assertSeeLink('Login')
            ->clickLink('Login')
            ->assertPathIs('/login')
            ->assertSee('Login Ini Olshop')
                ->assertInputPresent('email')
                ->assertInputPresent('password')
                ->assertSeeIn('button', 'Login')
                ->type('email', 'beer.ebba@example.org')
                ->type('password', '123')
                ->press('Login')
                ->assertSeeLink('Logout')
                ->clickLink('Logout')
                ->assertPathIs('/');
        });
    }
}
