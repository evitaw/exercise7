<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CartTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
            ->assertPathIs('/')
            ->assertSeeLink('Login')
            ->clickLink('Login')
            ->assertPathIs('/login')
            ->assertSee('Login Ini Olshop')
                ->assertInputPresent('email')
                ->assertInputPresent('password')
                ->assertSeeIn('button', 'Login')
                ->type('email', 'nbergnaum@example.net')
                ->type('password', '123')
                ->press('Login')
                ->assertSeeLink('Beli')
            ->clickLink('Beli')
            ->press('Keranjang')
            ->assertSeeLink('Lihat Semua')
            ->clickLink('Lihat Semua')
            ->assertPathIs('/cart');
        });
    }
}
