<?php

use Illuminate\Support\Facades\Route;
use \App\Models\User;
use \App\Models\Customer;
use \App\Mail\WelcomeMail;
use \App\Notifications\WelcomeNotification;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    $customer = Customer::all();

    $emails = ['nama1@email.com', 'nama2@email.com', 'nama3@email.com', 'nama4@email.com', 'nama5@email.com'];
    Mail::to($emails)->send(new WelcomeMail($customer));

    // return new WelcomeMail($user);
    // dd($user->notifications);
    // $user->notify(new WelcomeNotification());

    // $user->notifications->markAsUnread();
    
    // $user->unreadNotifications->markAsUnread();

    //return view('welcome');

    //return new WelcomeMail($user);

    //Mail::to($emails)->queue(new WelcomeMail($user));

});

Route::get('/send', function () {

    $message = 'Hello World!';

    event(new App\Events\NewMessage($message));

});

